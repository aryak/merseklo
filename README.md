# Merseklo
[![Join our Matrix](https://matrix.to/img/matrix-badge.svg)](https://matrix.to/#/#merseklo-space:envs.net)

A simple Matrix moderation bot written in BASH.

## Features

- Minimal dependencies (bash, curl & jq)

- Firewall (Prevents new users from entering)

- Invite system (Invite people to the rooms they tried to join and/or all the rooms.

- Kick, Ban, Unban and Message Removal Functionality 

- Remove all messages from a user is coming soon TM

## Usage

Add your directories in the sudo.sh file for logs and such. Add mods to your mod folder.

To run the bot, you will need to create 3 files. an example for each file is shown below:

```
$HOME/.config/merseklo/config

BaseUrl=https://matrix-client.matrix.org # Full HS name (you can get it at https://hs.xyz/.well-known/matrix/server)
AccessToken=syt_asdfasdfasdfasdfasdasdf # Token for account (All Settings -> Help & About -> Access Token)
User=@mybot:matrix.org # (Username of the user)
CheckRoomIntervalSec=1 # (How often should sudobot check for new messages, keep it at 1 to avoid being rate limited)
InitCmd=mersek # what should be typed to wake up merseklo
```

```
$HOME/.config/merseklo/roomlist
!klmnopqrst:matrix.org=Description or name of the room
!abcdefghij:matrix.org=Description or name of another room
```
> How can you use @ in a varname, bash doesn't support it right.

We do not use bash environment variables for the configuration file. Currently we are using [BAAM](https://discourse.destinationlinux.network/t/best-way-to-simulate-multidimensional-arrays-objects-in-bash/3149/26).
This might change in the future as BAAM might confuse many users and makes the code a bit more hard to read.
```
$HOME/.local/share/merseklo/mods
@account:homeserver.tld=invite,kick,ban,unban,groups,rm,firewall,sleep,poweroff,echo
@account2:homeserver.tld=invite,kick,ban,unban,groups,rm
```

When these files are created, run `merseklo`, and the bot should go online. When the script starts, test it by typing `sudo groups` into the room its in. If you are in the mods folder, it should provide a list of permissions you have, for example:

```
@account:homeserver.tld : [Permissions Here]
```

## TODO
- Split up the code to make it easier to read
- Make a module system

## Credits
Ulfnic - Creating the base bot and helping us along the way

Slips - Helping with Documentation and Code

Cobra - Persuading ulfnic to release source code :)
